#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
set -o errtrace

set -o noclobber

module=$(basename -s .jl $PWD)
mkdir -p src test docs/src docs/build
echo "docs/build/" > .gitignore
echo -e "module ${module}\n\nend" > src/${module}.jl
cat << EOF > test/runtests.jl
#!/usr/bin/env julia --color=yes

using Test
using ${module}

@test 1 == 1
EOF
cat << EOF > docs/make.jl
#!/usr/bin/env julia --color=yes
using Documenter
using ${module}

makedocs(
   modules = [${module}],
   format = :html,
   sitename = "${module}.jl",
   pages = ["index.md"]
)
EOF
chmod a+x test/runtests.jl docs/make.jl
cat << EOF > docs/src/index.md
# Documentation

\`\`\`@meta
CurrentModule = ${module}
DocTestSetup = quote
    using ${module}
end
\`\`\`

\`\`\`@contents
\`\`\`

\`\`\`@autodocs
Modules = [${module}]
\`\`\`

\`\`\`@index
\`\`\`
EOF
