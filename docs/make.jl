#!/usr/bin/env julia --color=yes
using Documenter
using CountPrimes

makedocs(
   modules = [CountPrimes],
   format = :html,
   sitename = "Julia Coding Dojo",
   pages = [
      "index.md",
      "Previous Episodes" => ["0.md"],
      "Setup" => filter(f -> match(r"^[1-3]\.md", f) != nothing,
                        readdir(joinpath(@__DIR__, "src"))),
      "docs.md",
      "Conclusion" => filter(f -> match(r"^[4-5]\.md", f) != nothing,
                             readdir(joinpath(@__DIR__, "src"))),

   ],
   assets = ["assets/primes.css"],
   html_disable_git = true
)
