# Documentation

```@meta
CurrentModule = CountPrimes
DocTestSetup = quote
    using CountPrimes
end
```

```@autodocs
Modules = [CountPrimes]
```

```@index
Pages = ["docs.md"]
```
