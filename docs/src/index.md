# CountPrimes - Julia Coding Dojo

```@contents
Pages = append!(filter(f -> match(r"^[0-3]\.md", f) != nothing, readdir()),
                append!(["docs.md"],
                        filter(f -> match(r"^[4-5]\.md", f) != nothing,
                               readdir())))
Depth = 1
```
