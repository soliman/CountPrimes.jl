#!/usr/bin/env julia --color=yes

using Test
using CountPrimes

@testset "isprime" begin
   @test !isprime(1)
   @test isprime(2)
   @test isprime(3)
   @test_throws MethodError isprime(3.5)
   @test !isprime(4)
   @test isprime(5)
end

range = 700000:1000000
collected_range = collect(range)
expected_range = 21955
collection = [collected_range; collected_range]
expected_collection = 2 * expected_range

@testset "count_primes" begin
   print("range  ")
   @time @test count_primes(range) == expected_range
   print("collect")
   @time @test count_primes(collected_range) == expected_range
   print("append ")
   @time @test count_primes(collection) == expected_collection
   @test_throws ArgumentError count_primes(nothing)
   print("dot    ")
   @time @test count_primes(collection, method="dot") == expected_collection
   print("map    ")
   @time @test count_primes(collection, method="map") == expected_collection
end

using Distributed
addprocs(3)
println(nworkers(), " workers")
@everywhere using CountPrimes

@testset "faster" begin
   print("memo   ")
   @time @test count_primes(collection, method="memo") == expected_collection
   print("distrib")
   @time @test count_primes(collection, method="distrib") == expected_collection
   print("pmap   ")
   @time @test count_primes(collection, method="pmap") == expected_collection
end
