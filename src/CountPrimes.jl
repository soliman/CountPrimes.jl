module CountPrimes

using Distributed

export isprime, count_primes

"""
   isprime(number)

tests if number is prime

# Examples
"""
function isprime(number::Integer)::Bool
   if number <= 1
      return false
   end
   for n = 2:sqrt(number)
      if number % n == 0
         return false
      end
   end
   true
end

function count_primes(collection; method="comprehension")
   if !applicable(iterate, collection)
      throw(ArgumentError("oulalal"))
   end
   if method == "comprehension"
      count([isprime(n) for n in collection])
   elseif method == "dot"
      count(isprime.(collection))
   elseif method == "map"
      count(map(isprime, collection))
   elseif method == "memo"
      memo = Dict{Number, Bool}()
      count(map(n -> isprime_memo(n, memo), collection))
   elseif method == "pmap"
      count(pmap(isprime, collection))
   elseif method == "distrib"
      @distributed (+) for n in collection
         isprime(n)
      end
   end
end

function isprime_memo(number::Integer, memo)::Bool
   get!(memo, number) do
      isprime(number)
   end
end

end
